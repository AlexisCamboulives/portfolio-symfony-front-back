<?php

namespace App\Controller;

use App\Entity\Projet;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;

class ProjetsController extends AbstractController
{
    #[Route('/projets', name: 'app_projets')]
    public function index(ManagerRegistry $doctrine): Response
    {

        $projets = $doctrine->getRepository(Projet::class)->findAll();

        return $this->render('projets/projets.html.twig', [
            'controller_name' => 'ProjetsController',
            'projets' => $projets
        ]);
    }
}
