<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Projet;

class AdminController extends AbstractController
{
    #[Route('/admin', name: 'app_admin')]
    public function index(ManagerRegistry $doctrine, Request $request): Response
    {
        
        $request = Request::createFromGlobals();
        $entityManager = $doctrine->getManager();
        if($request->request->get('title')){
        $addProjet = new Projet();
        $addProjet->setTitle($request->request->get('title'));
        $addProjet->setDescription($request->request->get('description'));
        $addProjet->setImgUrl1($request->request->get('imgUrl1'));
        $addProjet->setImgurl2($request->request->get('imgUrl2'));
        $addProjet->setLienRepo($request->request->get('lienRepo'));
        $addProjet->setLienSite($request->request->get('lienSite'));
        $entityManager->persist($addProjet);
        $entityManager->flush();
        }
        return $this->render('admin/admin.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }
    
}
